package com.example.user.ai;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String database_name="register.db";
    public static final String table_name="users";
    public static final String col_id="id";
    public static final String col_name="name";
    public static final String col_email="email";
    public static final String col_location ="location";
    public static final String col_password ="password";
    Cursor cursor;


    public DatabaseHelper(Context context) {
        super(context, database_name, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
   db.execSQL("CREATE TABLE users(id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT,email TEXT,location TEXT,password TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
db.execSQL("DROP TABLE IF EXISTS "+table_name);
onCreate(db);
    }
    public long adduser(String myemail ,String mylocation,String myname,String mypassword){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put("name",myname);
        contentValues.put("email",myemail);
        contentValues.put("location",mylocation);
        contentValues.put("password",mypassword);
       long res=db.insert("users",null,contentValues);
        db.close();
        return res;
    }
    public Boolean checkuser( String user,String pwd){
        String[] columns={col_id};
        SQLiteDatabase db = getWritableDatabase();
        String selection=  col_email + "=?" + " AND " + col_password + " =? ";
        String [] selectionArgs={ user , pwd };
        cursor=db.query( table_name, columns, selection, selectionArgs,null,null,null);
        int count= cursor.getCount();
        cursor.close();
        db.close();
        if(count > 0)
            return true;

            else
                return  false;


    }
}

