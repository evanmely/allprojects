package com.example.user.ai;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class login extends AppCompatActivity {
    EditText myemail;
    EditText mypassword;
    Button btnlogin;
    TextView register;
   DatabaseHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        db=new DatabaseHelper(this);
        myemail=(EditText) findViewById(R.id.email);
        mypassword=(EditText)findViewById(R.id.password);
        btnlogin=(Button)findViewById(R.id.login);
        register=(TextView)findViewById(R.id.register);

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user=myemail.getText().toString().trim();
                String pwd= mypassword.getText().toString().trim();
                Boolean res=db.checkuser(user,pwd);

               if(res==true) {
                   Toast.makeText(login.this, " LOGIN SUCCESS --WELCOME", Toast.LENGTH_SHORT).show();
                Intent movetolanding = new Intent(login.this,landing.class);
               startActivity(movetolanding);
               }
               else
               {

                   Toast.makeText(login.this, " Please Check your Username or Password ", Toast.LENGTH_SHORT).show();
               }
               }
        });
    }
}
