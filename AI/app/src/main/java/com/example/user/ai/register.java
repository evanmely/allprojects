package com.example.user.ai;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class register extends AppCompatActivity {
Button mybtnsave;
EditText myname;
EditText myemail;
EditText mylocation;
EditText pass;
EditText cnfpass;
TextView mylogin;
DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        db=new DatabaseHelper(this);
        myname=(EditText)findViewById(R.id.name);
        myemail=(EditText)findViewById(R.id.email);
        mylocation=(EditText)findViewById(R.id.location);
        pass=(EditText)findViewById(R.id.password);
        cnfpass=(EditText)findViewById(R.id.confirmpassword);
        mylogin=(TextView)findViewById(R.id.btnlogin);
        mybtnsave=(Button)findViewById(R.id.btnsave);
        mylogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent saveIntent=new Intent(register.this,MainActivity.class);
                startActivity(saveIntent);
            }
        });
        mybtnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email=myemail.getText().toString().trim();
                String  name=myname.getText().toString().trim();
                String location=mylocation.getText().toString().trim();
                String pwd=pass.getText().toString().trim();
                String cpwd=cnfpass.getText().toString().trim();
                if(pwd.equals(cpwd)){
                   long val=db.adduser(name,email,location,pwd);
                   if(val > 0){
                       Toast.makeText(register.this, " Registration Successful", Toast.LENGTH_SHORT).show();
                       Intent movetologin=new Intent(register.this,MainActivity.class);
                       startActivity(movetologin);
                   }
                   else{
                       Toast.makeText(register.this, " Registration Error", Toast.LENGTH_SHORT).show();
                   }

                }
                else{
                    Toast.makeText(register.this, "Your passwords don't match", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
