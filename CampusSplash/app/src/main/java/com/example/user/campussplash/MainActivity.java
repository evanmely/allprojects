package com.example.user.campussplash;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.example.user.campussplash.*;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
