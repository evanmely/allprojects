package com.example.user.househelps;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button  househelp,employer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        employer= (Button)findViewById(R.id.employer);
        househelp= (Button)findViewById(R.id.househelp);
        employer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent log= new Intent(getApplicationContext(),login.class);
                //Intent log= new Intent(getApplicationContext(),househelplisting.class);
                startActivity(log);
            }
        });
    }
}

