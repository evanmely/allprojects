package com.example.user.househelps.controllers;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.user.househelps.R;
import com.example.user.househelps.househelp_details;
import com.example.user.househelps.model.househelps_model;

import java.util.List;

public class househelps_adapter extends  RecyclerView.Adapter<househelps_adapter.househelpsViewHolder>  {
    private Context ctx;
    private List<househelps_model>HouseList;
    private OnItemClickListener mlistener;
    public interface OnItemClickListener{
        void OnItemClick(int position);
    }
public  void setOnItemClickListener(OnItemClickListener listener){
        mlistener= listener;

}
    public househelps_adapter(Context ctx, List<househelps_model> HouseList) {
        this.ctx = ctx;
        this.HouseList = HouseList;
    }
    @NonNull
    @Override
    public househelpsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(ctx);
        View view = inflater.inflate(R.layout.view_househelps, null);
        return new househelpsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull househelpsViewHolder holder, int i) {
         final househelps_model hh = HouseList.get(i);
         //byte[] decodedString = Base64.decode(hh.getImage(), Base64.DEFAULT);
       // ;
//        Glide.with(ctx)
//               .load(decodedString)
//                .thumbnail(0.5f)
//                .crossFade()
//                .fitCenter()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//               .into(holder.imageView);


       // holder.imageView.setImageBitmap(bitmap);
        holder.name.setText(hh.getFirst_name());
        holder.textdob.setText(hh.getDob());
        holder.texttype.setText(String.valueOf(hh.getType()));
        holder.textsalary.setText(String.valueOf(hh.getSalary()));
        holder.textStatus.setText(String.valueOf(hh.getSalary()));
        //holder.imageView.buildDrawingCache();
        //final Bitmap bmp = holder.imageView.getDrawingCache();
        //final Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(ctx,househelp_details.class);
                intent.putExtra("name",hh.getFirst_name());
                intent.putExtra("dob",String.valueOf(hh.getDob()));
                intent.putExtra("marital_status",hh.getMarital_status());
                intent.putExtra("type",hh.getType());
                intent.putExtra("expected_salary",String.valueOf(hh.getSalary()));
                ctx.startActivity(intent);
            }
        });
        //holder.imageView.setImageDrawable(ctx.getResources().getDrawable(hh.getImage()));
    }
    @Override
    public int getItemCount() {
        return HouseList.size();
    }

    class househelpsViewHolder extends  RecyclerView.ViewHolder{
        TextView name, textdob, texttype, textsalary,textStatus;
       ImageView imageView;

     public househelpsViewHolder(@NonNull View itemView) {
         super(itemView);
         name = itemView.findViewById(R.id.name);
         textdob = itemView.findViewById(R.id.dob);
         textsalary = itemView.findViewById(R.id.salary);
         textStatus=itemView.findViewById(R.id.status);
         texttype= itemView.findViewById(R.id.type);
         imageView = itemView.findViewById(R.id.imageView);
         imageView.buildDrawingCache();
         Bitmap bmp = imageView.getDrawingCache();
         itemView.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 if(mlistener!=null){
                     int position= getAdapterPosition();
                     if(position!=RecyclerView.NO_POSITION){
                         mlistener.OnItemClick(position);

                     }
                 }
             }
         });
     }
 }


}
