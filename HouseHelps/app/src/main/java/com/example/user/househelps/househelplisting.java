package com.example.user.househelps;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.user.househelps.controllers.househelps_adapter;
//import com.example.user.househelps.helper.HttpJsonParser;
import com.example.user.househelps.model.househelps_model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class househelplisting extends AppCompatActivity {

    public  static  final  String EXTRA_URL0="image";
    public  static  final  String EXTRA_URL="image";
    public  static  final  String EXTRA_NAME="name";
    public  static  final  String EXTRA_LOCATION="location";
    public  static  final  String EXTRA_SALARY= "expected_salary";

    String MAIN_URL = "http://192.168.0.215/api/househelps";
    List<househelps_model> houseList;


    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_househelplisting);
        //getting the recyclerview from xml
        //recyclerView = findViewById(R.id.recyclerview);
        recyclerView = findViewById(R.id.recyclerview);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);
        //initializing the houseList
        houseList = new ArrayList<>();

        //this method will fetch and parse json
        //to display it in recyclerview
        loadHousehelps();

    }
        private void loadHousehelps () {
            StringRequest stringRequest = new StringRequest(Request.Method.GET, MAIN_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //converting the string to json array object
                                JSONObject obj = new JSONObject(response);

                                JSONArray array = obj.getJSONArray("data");
                               Toast.makeText(househelplisting.this, "Data Received " + array, Toast.LENGTH_LONG).show();


                                //traversing through all the object
                                for (int i = 0; i < array.length(); i++) {

                                    //getting product object from json array
                                    JSONObject househelp = array.getJSONObject(i);
                                    //Log.e(" error",househelp.toString());
                                    //adding the househelps to  view
                                    houseList.add(new househelps_model(
                                            househelp.getInt("id"),
                                            househelp.getString("first_name"),
                                            househelp.getString("marital_status"),
                                            househelp.getString("dob"),
                                            househelp.getDouble("salary_expectation"),
                                            househelp.getString("type_of_placement")

                                    ));
                                }

                                //creating adapter object and setting it to recyclerview
                                househelps_adapter adapter = new househelps_adapter(househelplisting.this, houseList);
                                recyclerView.setAdapter(adapter);
                               // adapter.setOnItemClickListener(househelplisting.this);
                            } catch (JSONException e) {
                                Toast.makeText(househelplisting.this, "ERROR" + e, Toast.LENGTH_LONG).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e(" error",error.toString());
                        }
                    });

            //adding our stringrequest to queue
            Volley.newRequestQueue(this).add(stringRequest);


        }

//    @Override
//    public void OnItemClick(int position) {
//Intent details= new Intent(househelplisting.this,househelp_details.class);
//househelps_model model= houseList.get(position);
//        details.putExtra(EXTRA_NAME,model.getName());
//        details.putExtra(EXTRA_URL,model.getImage());
//        details.putExtra(EXTRA_LOCATION,model.getLocation());
//        details.putExtra(EXTRA_SALARY,model.getSalary());
//        startActivity(details);
//
//    }
}