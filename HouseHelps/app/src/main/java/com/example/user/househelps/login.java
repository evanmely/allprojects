package com.example.user.househelps;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.textclassifier.TextLinks;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.user.househelps.APIs.URLs;
import com.example.user.househelps.controllers.SharedPrefmanager;
import com.example.user.househelps.controllers.VolleySingleton;
import com.example.user.househelps.model.User_model;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class login extends AppCompatActivity {
Button log,reg;
    ProgressBar myprogressBar;
EditText myemail,mypassword;
final String login_url="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        myemail=(EditText)findViewById(R.id.myemail);
        mypassword=(EditText)findViewById(R.id.password);
        log= (Button)findViewById(R.id.login);
        reg= (Button)findViewById(R.id.btnregister);
        myprogressBar=(ProgressBar)findViewById(R.id.progressBar);
        if (SharedPrefmanager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, househelplisting.class));
        }
        log.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent house= new Intent(getApplicationContext(),househelplisting.class);
                startActivity(house);
                //userLogin();
        }
        }
        );

        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registration = new Intent(getApplicationContext(),com.example.user.househelps.registration.class);
                startActivity(registration);
            }
        });


    }

        private void userLogin() {
            //first getting the values
            final String username = myemail.getText().toString();
            final String password = mypassword.getText().toString();

            //validating inputs
            if (TextUtils.isEmpty(username)) {
                myemail.setError("Please enter your username");
                myemail.requestFocus();
                return;
            }

            if (TextUtils.isEmpty(password)) {
                mypassword.setError("Please enter your password");
                mypassword.requestFocus();
                return;
            }

            //if everything is fine
            StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_LOGIN,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            myprogressBar.setVisibility(View.GONE);

                            try {
                                //converting response to json object
                                JSONObject obj = new JSONObject(response);

                                //if no error in response
                                if (!obj.getBoolean("error")) {
                                    Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();

                                    //getting the user from the response
                                    JSONObject userJson = obj.getJSONObject("user");

                                    //creating a new user object
                                    User_model user = new User_model(
                                            userJson.getInt("id"),
                                            userJson.getString("name"),
                                            userJson.getString("email"),
                                            userJson.getString("phone")
                                    );

                                    //storing the user in shared preferences
                                    SharedPrefmanager.getInstance(getApplicationContext()).userLogin(user);

                                    //starting the hoi activity
                                    finish();
                                    startActivity(new Intent(getApplicationContext(), househelplisting.class));
                                } else {
                                    Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("name", username);
                    params.put("password", password);
                    return params;
                }
            };

            VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
        }
}

