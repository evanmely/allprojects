package com.example.user.househelps;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.user.househelps.APIs.URLs;
import com.example.user.househelps.controllers.SharedPrefmanager;
import com.example.user.househelps.controllers.VolleySingleton;
import com.example.user.househelps.model.User_model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Map;

public class registration extends AppCompatActivity {
    EditText name, email, phone,password,password1;
    ProgressBar progressBar;
    TextView btnlog;
    String regurl = "http://192.168.0.215/api/register";
    ProgressDialog progressDialog;
    Button save;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        btnlog=(TextView)findViewById(R.id.btnlogin);
        name =(EditText)findViewById(R.id.name);
        email=(EditText)findViewById(R.id.email);
        phone=(EditText)findViewById(R.id.phone);
        password=(EditText)findViewById(R.id.password);
        password1=(EditText)findViewById(R.id.cfpassword);
        save=(Button)findViewById(R.id.btnsave);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);


        //if the user is already logged in we will directly start the profile activity
        if (SharedPrefmanager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, househelplisting.class));
            return;
        }


        btnlog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login = new Intent(getApplicationContext(),com.example.user.househelps.login.class);
                startActivity(login);
            }
        });
save.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        registerUser();
    }
});
    }
    private void registerUser() {
        final String username = name.getText().toString().trim();
        final String emailaddr = email.getText().toString().trim();
        final String phonenumber=phone.getText().toString().trim();
        final String pass1=password1.getText().toString().trim();
        final String pass = password.getText().toString().trim();

        //first we will do the validations

        if (TextUtils.isEmpty(username)) {
            name.setError("Please enter username");
            name.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(emailaddr)) {
            email.setError("Please enter your email");
            email.requestFocus();
            return;
        }

        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailaddr).matches()) {
            email.setError("Enter a valid email");
            email.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(pass)) {
            password.setError("Enter a password");
            password.requestFocus();
            return;
        }
        if (!pass.matches(pass1)) {
            password.setError("Passwords do not match");
            password.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(phonenumber)) {
            password.setError("Enter a phone number");
            password.requestFocus();
            return;
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.REG_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);

                        try {
                            //converting response to json object
                            JSONObject obj = new JSONObject(response);

                            //if no error in response
                            if (!obj.getBoolean("false")) {
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();

                                //getting the user from the response
                                JSONObject userJson = obj.getJSONObject("user");

                                //creating a new user object
                                User_model user = new User_model(
                                        userJson.getInt("id"),
                                        userJson.getString("name"),
                                        userJson.getString("email"),
                                        userJson.getString("phone")
                                );

                                //storing the user in shared preferences
                                SharedPrefmanager.getInstance(getApplicationContext()).userLogin(user);

                                //starting the profile activity
                                finish();
                                startActivity(new Intent(getApplicationContext(),househelplisting.class));
                            } else if(obj.getBoolean("false")){
                              JSONObject object = new JSONObject(response);
                                JSONObject error =  new JSONObject( object.getString("error"));
                                JSONArray ja=error.getJSONArray("email");
                    switch(object.getString("error")){

                        case "email":{
                            Toast.makeText(registration.this,ja.getString(0), Toast.LENGTH_LONG).show();
                        }
                        case "phone":{
                            Toast.makeText(registration.this,ja.getString(1), Toast.LENGTH_LONG).show();

                        }
                        case "password":{
                            Toast.makeText(registration.this,error.getString("password"), Toast.LENGTH_LONG).show();

                        }

                                }
                                Toast.makeText(getApplicationContext(), obj.getString("error"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("name", username);
                params.put("email", emailaddr);
                params.put("phone", phonenumber);
                params.put("password", pass);
                return params;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }

}
