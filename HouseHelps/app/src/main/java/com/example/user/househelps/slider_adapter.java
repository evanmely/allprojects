package com.example.user.househelps;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class slider_adapter  extends PagerAdapter {

    Context mcontext;
    LayoutInflater layoutInflater;

    public slider_adapter(Context mcontext) {
        this.mcontext = mcontext;
    }
    public  int[] slide_images ={
            R.drawable.background1,
            R.drawable.background,
            R.drawable.background2

    };
    public  String [] slide_headings ={
            "About" ,
            "Usage",
            "Vision"

    };
    public  String [] slide_descriptions ={
            "The application helps people especially those in urban areas to search and request caregivers and househelps of their choice" ,
            " You register and then login to access app dashboard",
            "To reduce the challenges of finding househelps and caregivers by enabling the users to request them just in a single tap"

    };

    @Override
    public int getCount() {
        return slide_headings.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view ==(RelativeLayout) o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater)mcontext.getSystemService(mcontext.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slide_layout,container,false);
        ImageView mslideimageview = (ImageView)view.findViewById(R.id.slide_image);
        TextView myheading = (TextView)view.findViewById(R.id.slide_heading);
        TextView  mydesc = (TextView)view.findViewById(R.id.slide_desc);
        mslideimageview.setImageResource(slide_images [position]);
        myheading.setText(slide_headings[position]);
        mydesc.setText(slide_descriptions[position]);
        container.addView(view);
        return  view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((RelativeLayout)object);

    }




}
