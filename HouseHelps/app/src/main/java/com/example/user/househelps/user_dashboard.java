package com.example.user.househelps;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class user_dashboard extends AppCompatActivity {
Button  househelp,employer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_dashboard);
        employer= (Button)findViewById(R.id.employer);
        househelp= (Button)findViewById(R.id.househelp);
        employer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent log= new Intent(getApplicationContext(),login.class);
                startActivity(log);
            }
        });
    }
}
