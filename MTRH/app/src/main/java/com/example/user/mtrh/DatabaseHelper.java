package com.example.user.mtrh;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.support.annotation.Nullable;

public class DatabaseHelper  extends SQLiteOpenHelper {
    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, null, 1);
    }


        @Override
    public void onCreate(SQLiteDatabase db) {

    }
    public void addpatient(String name, String patid, String password) {
        SQLiteDatabase database = getWritableDatabase();
        String sql = "INSERT INTO patient VALUES(NULL,?,?,?)";
        SQLiteStatement statement = database.compileStatement(sql);
        statement.clearBindings();
        statement.bindString(1, name);
        statement.bindString(2, patid);
        statement.bindString(3, password);
        statement.executeInsert();
    }
    public void querydata(String sql) {
        SQLiteDatabase database = getWritableDatabase();
        database.execSQL(sql);

    }
    public boolean verify(String username, String password) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("select * from patient where pid=? and password=?", new String[]{username, password});
        int count = c.getCount();
        if (count > 0)
            return true;

        else
            return false;

    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
