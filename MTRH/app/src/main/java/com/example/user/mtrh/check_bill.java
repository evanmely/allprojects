package com.example.user.mtrh;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.user.mtrh.helper.HttpJsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class check_bill extends Fragment {
    private static final String AMOUNT = "Amount";
    Button btnpay;
    private static String STRING_EMPTY = "";
    private static final String PHONE = "PhoneNumber";
    private static final String BASE_URL = "http://192.168.0.194:8080/mpesa_express/pay1.php";
    private EditText amount,phone;
    private String amount_pay;
    private String mobile_number;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       btnpay=(Button)getView().findViewById(R.id.btnPay);
       amount=(EditText)getView().findViewById(R.id.eAmount);
       phone=(EditText)getView().findViewById(R.id.eNumber);
      btnpay.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              pay();
          }
      });
        return inflater.inflate(R.layout.check_bill_layout,container,false);
    }
    private void pay() {
        if (!STRING_EMPTY.equals(amount.getText().toString()) &&
                !STRING_EMPTY.equals(phone.getText().toString())){
            mobile_number = phone.getText().toString();
            amount_pay = amount.getText().toString();

        }
        else{
            Toast.makeText(getContext(), "\"Please Make sure all fields are not empty", Toast.LENGTH_SHORT).show();
        }

    }
    @Override
    protected String doInBackground(String... params) {
        HttpJsonParser httpJsonParser = new HttpJsonParser();
        Map<String, String> httpParams = new HashMap<>();
        //Populating request parameters
        httpParams.put(AMOUNT, amount_pay);
        httpParams.put(PHONE, mobile_number);
        JSONObject jsonObject = httpJsonParser.makeHttpRequest(
                BASE_URL, "POST", httpParams);

        return null;
    }

}
