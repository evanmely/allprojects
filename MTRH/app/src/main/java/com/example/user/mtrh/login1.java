package com.example.user.mtrh;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class login1 extends AppCompatActivity {
    Button btnlogin,btnreg;
    DatabaseHelper db;
    TextView forgotpwd;
    EditText id ,mypassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login1);
        btnlogin=(Button)findViewById(R.id.login);
        btnreg=(Button)findViewById(R.id.btnregister);
        id=(EditText)findViewById(R.id.myid);
        mypassword=(EditText)findViewById(R.id.password);
        db=new DatabaseHelper(this,"mtrh.sqlite",null,1);
        btnreg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent register= new Intent(getApplicationContext(),registration.class);
                startActivity(register);
            }
        });
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    boolean test = db.verify(id.getText().toString(),mypassword.getText().toString());
                    if (test == true) {
           Intent pharm= new Intent(getApplicationContext(),displ.class);
                 startActivity(pharm);
                    } else {
                        Toast.makeText(login1.this, "INCORRECT CREDENTIALS", Toast.LENGTH_LONG).show();
                    }

                }
                catch(Exception r){
                    Toast.makeText(login1.this, "" + r, Toast.LENGTH_LONG).show();
                }
            }

        });
    }
}
