package com.example.user.mtrh;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class registration extends AppCompatActivity {
    EditText myname,myid,mypass;
    Button save;
    DatabaseHelper db;
    TextView mylogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        db = new DatabaseHelper(this, "mtrh.sqlite", null, 1);
        db.querydata("CREATE TABLE IF NOT EXISTS patient(id INTEGER PRIMARY KEY AUTOINCREMENT,name VARCHAR , pid VARCHAR,password VARCHAR)");
        myname = (EditText) findViewById(R.id.name);
        myid = (EditText)findViewById(R.id.patid);
        mypass = (EditText) findViewById(R.id.password);
        save = (Button) findViewById(R.id.btnsave);
        mylogin = (TextView)findViewById(R.id.btnlogin);
        mylogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mylog= new Intent(getApplicationContext(),login1.class);
                startActivity(mylog);
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    db.addpatient(
                            myname.getText().toString().trim(),
                            myid.getText().toString().trim(),
                            mypass.getText().toString().trim()
                    );
                    Toast.makeText(getApplicationContext(), "Data successfully savedj", Toast.LENGTH_LONG).show();
                    //Toast.makeText(register.this, " Data Saved successfully", Toast.LENGTH_LONG).show();
                    myname.setText("");
                    mypass.setText("");
                    myid.setText("");

                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "" + e, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    }

