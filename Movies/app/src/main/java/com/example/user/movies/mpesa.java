package com.example.user.movies;
import android.app.DownloadManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;


public class mpesa extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mpesa);
    }

    OkHttpClient client = new OkHttpClient();

    MediaType mediaType = MediaType.parse("application/json");
    RequestBody body = RequestBody.create(mediaType, "{\"BusinessShortCode\": \" \",
            \"Password\": \" \",
            \"Timestamp\": \" \",
            \"TransactionType\": \"CustomerPayBillOnline\",
            \"Amount\": \" \",
            \"PartyA\": \" \",
            \"PartyB\": \" \",
            \"PhoneNumber\": \" \",
            \"CallBackURL\": \"https://ip_address:port/callback\",
            \"AccountReference\": \" \",
            \"TransactionDesc\": \" \"}");
    DownloadManager.Request request = new Request.Builder()
            .url("https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest")
            .post(body)
            .addHeader("authorization", "Bearer ACCESS_TOKEN")
            .addHeader("content-type", "application/json")
            .build();

    Response response = client.newCall(request).execute();


}
