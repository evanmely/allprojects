package com.example.user.sortmessages;
import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.view.Display;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends ListActivity {

    protected BroadcastReceiver myReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            final Bundle bundle = intent.getExtras();
            try {
                if (bundle != null) {
                    final Object[] pdusObj = (Object[]) bundle.get("pdus");
                    for (int i = 0; i < pdusObj.length; i++) {

                        SmsMessage currentMessage;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            String format = bundle.getString("format");
                            currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i], format);
                            Log.e("Current Message", format + " : " + currentMessage.getDisplayOriginatingAddress());
                        } else {
                            currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                        }
                        Pattern regEx =
                                Pattern.compile("[a-zA-Z0-9]{2}-[a-zA-Z0-9]{6}");
                        Matcher m = regEx.matcher(currentMessage.getDisplayOriginatingAddress());
                        if (m.find()) {
                            try {
                                String phoneNumber = m.group(0);
                                Long date = currentMessage.getTimestampMillis();
                                String message = currentMessage.getDisplayMessageBody();
                                Log.e("SmsReceiver Mine", "senderNum: " + phoneNumber + "; message: " + message);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Log.e("Mismatch", "Mismatch value");
                        }
                    }
                }
            } catch (Exception e) {
                Log.e("SmsReceiver", "Exception smsReceiver" + e);
            }

        }
    };
}

