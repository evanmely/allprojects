package com.example.user.sortmessages;

public class Model  {
    String amount,acc_no,date_time;

    public Model( String amount, String acc_no, String date_time) {
        //this.id = id;
        this.amount = amount;
        this.acc_no = acc_no;
        this.date_time = date_time;
    }

//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }

    public  String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public  String getAcc_no() {
        return acc_no;
    }

    public void setAcc_no(String acc_no) {
        this.acc_no = acc_no;
    }

    public String getDate() {
        return date_time;
    }

    public void setDate(String date) {
        this.date_time = date;
    }
}


