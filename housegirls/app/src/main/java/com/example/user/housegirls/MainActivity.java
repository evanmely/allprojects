package com.example.user.housegirls;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    String HttpUrl = "192.168.0.194:80/MPESA/payment.php";

    private EditText amountEditText;
    private EditText phoneEditText;
   ProgressDialog progressDialog;
    Button pay,profile;
    //String phoneHolder,amountHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        phoneEditText = (EditText) findViewById(R.id.phone);
        amountEditText = (EditText) findViewById(R.id.amount);
        pay = (Button) findViewById(R.id.btnpay);
        profile = (Button) findViewById(R.id.btnprof);
profile.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent prof= new Intent(getApplicationContext(),profile.class);
        startActivity(prof);
    }
});
                            pay.setOnClickListener(new View.OnClickListener()

                    {
                        @Override
                        public void onClick (View v){

                            loginUser();
                           new  payAsyncTask().execute();


                  }
                   });

                }
    private void loginUser(){

        final String phone = phoneEditText.getText().toString().trim();
        final String amount = amountEditText.getText().toString().trim();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,HttpUrl ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("phone",phone);
                params.put("amount",amount);

                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

     private class payAsyncTask extends AsyncTask<String, String, String> {
         @Override
         protected String doInBackground(String... strings) {
             return null;
         }

         @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Display progress bar
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("Paying.... Please wait.");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {

                }
            });
        }

    }

}




