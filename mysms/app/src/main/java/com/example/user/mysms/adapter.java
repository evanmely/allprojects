package com.example.user.mysms;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class adapter extends BaseAdapter {
    private Context context;
    private  int layout;
    private ArrayList<model> arrayList;

    public adapter(Context context, int layout, ArrayList<model> arrayList) {
        this.context = context;
        this.layout = layout;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    private  class ViewHolder{
        TextView txtamount,txtacc,txtdate;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View row=view;
        adapter.ViewHolder holder = new adapter.ViewHolder();
        if(row==null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout,null);
            holder.txtamount = row.findViewById(R.id.t_amount);
            holder.txtdate = row.findViewById(R.id.t_date);
            holder.txtacc = row.findViewById(R.id.t_acc_no);
            row.setTag(holder);
    }
    else{
            holder = (ViewHolder)row.getTag();
        }
        model  mymodel = arrayList.get(position);
        holder.txtacc.setText(String.valueOf(mymodel.getAcc_no()));
        holder.txtamount.setText(mymodel.getAmount());
        holder.txtdate.setText(mymodel.getDate());
        return row;
    }
}
