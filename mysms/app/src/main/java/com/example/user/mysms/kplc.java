package com.example.user.mysms;

import android.database.Cursor;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class kplc extends AppCompatActivity {
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 123;
    ListView mlistview;
    adapter mAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kplc);
        ActivityCompat.requestPermissions(kplc.this, new String[]{"android.permission.READ_SMS"}, REQUEST_CODE_ASK_PERMISSIONS);
        mlistview = (ListView) findViewById(R.id.smsListview);

        try {
            //mList = new ArrayList<>();
            // mAdapter = new adapter(this,R.layout.row,mList);
            if (fetchInbox() != null) {
                mAdapter = new adapter(this, R.layout.row, fetchInbox());
                // ArrayAdapter<model> mAdapter = new adapter(this,R.layout.row, fetchInbox());
                mlistview.setAdapter(mAdapter);
            }
        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), "" + ex, Toast.LENGTH_LONG).show();
        }
    }
    public double getTotal(ArrayList<model> list){

        double total=0.0;
        for(int i=0;i<list.size();i++){
            total=total+Double.parseDouble(list.get(i).getAmount());
        }
        return total;
    }

    public ArrayList<model> fetchInbox() {
        ArrayList<model> mList = new ArrayList<>();
        Uri uri = Uri.parse("content://sms/inbox");
        Cursor cursor = getContentResolver().query(uri, new String[]{"_id", "address", "person", "body", "date", "type"}, "address='KENYA POWER'", null, "date desc");
        cursor.moveToFirst();

        while (cursor.moveToNext()) {



            //String data = "MPESA ND22X3HC8E Confirmed. Ksh500.00 sent to KPLC PREPAID  for accoun 2/4/19 at 12:27 PM New M-PESA balance is Ksh224.14. Transaction cost, Ksh23.00.";
            String data = cursor.getString(3);
            String data1 = cursor.getString(3);
            Pattern pattern = Pattern.compile("(?i)(?:(?:RS|Ksh|MRP)\\.?\\s?)(\\d+(:?\\,\\d+)?(\\,\\d+)?(\\.\\d{1,2})?)");
            Pattern pattern1 = Pattern.compile("(\\b\\d{11})");
            Matcher matcher1 = pattern1.matcher(data1);
            Matcher matcher = pattern.matcher(data);
            String val = "";
            String val2 = "";
            if (matcher.find() && (matcher1.find())) {
                //for (int i = 0; i < matcher.groupCount() + 1; i++)
                val = matcher.group(1);
                val2 = matcher1.group(1);
                // val2= matcher.group(1);

            }


            //                Matcher m = regEx.matcher("MPESA");
//                if(m.find());
//                String val= m.group(1);
            String address = cursor.getString(1);
            String body = cursor.getString(3);
            String type = cursor.getString(5);
            //String date = cursor.getString(cursor.getColumnIndexOrThrow("date")).toString();
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Calendar calendar = Calendar.getInstance();
            long now = cursor.getLong(4);
            calendar.setTimeInMillis(now);
            //sms.add(address+"\n\n"+body+"\n\n"+date);
            //if(body.contains("KPLC PREPAID")) {
            mList.add(new model(val, Double.parseDouble(val2), formatter.format(calendar.getTime())));
            // }
        }

        return mList;
    }
}







