package com.example.user.mysms;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.nfc.Tag;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class main2 extends AppCompatActivity {
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 123;
    ListView mlistview;
    adapter mAdapter = null;
    TextView mytextview;
    public  String date=null;
    EditText edittext;

    Cursor cursor=null;
    final Calendar myCalendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        setContentView(R.layout.activity_main2);
        //datePicker = (DatePicker) findViewById(R.id.dpResult);
        edittext= (EditText) findViewById(R.id.day);
         final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };

         try {
             edittext.setOnClickListener(new View.OnClickListener() {

                 @Override
                 public void onClick(View v) {
                     // TODO Auto-generated method stub
                     new DatePickerDialog(main2.this, date, myCalendar
                             .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                             myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                 }
             });
         }
         catch (Exception e){
             Toast.makeText(this, ""+e, Toast.LENGTH_SHORT).show();
         }

            // datePicker.findViewById(Resources.getSystem().getIdentifier("day", "id", "android")).setVisibility(View.GONE);
        mytextview = (TextView) findViewById(R.id.total);
        ActivityCompat.requestPermissions(main2.this, new String[]{"android.permission.READ_SMS"}, REQUEST_CODE_ASK_PERMISSIONS);
        mlistview = (ListView) findViewById(R.id.smsListview);

        try {
            if (fetchInbox() != null) {
                mAdapter = new adapter(this, R.layout.row, fetchInbox());
                // ArrayAdapter<model> mAdapter = new adapter(this,R.layout.row, fetchInbox());
                mlistview.setAdapter(mAdapter);
            }
        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), "" + ex, Toast.LENGTH_LONG).show();
        }
    }
    public void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.UK);
    date =sdf.format(myCalendar.get(Calendar.MONTH));
        edittext.setText(sdf.format(myCalendar.getTime()));
    }
    public ArrayList<model> fetchInbox() {
        ArrayList<model> mList = new ArrayList<>();
        Uri uri = Uri.parse("content://sms/inbox");
        try {
            //SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
// Create a SimpleDateFormat object.

             cursor = getContentResolver().query(uri, new String[]{"_id", "address", "person", "body", "date", "type"},"address='MPESA'",null, null );
            cursor.moveToFirst();
            while (cursor.moveToNext()) {
                String data = cursor.getString(3);
                String data1 = cursor.getString(3);
                Pattern pattern = Pattern.compile("(?i)(?:(?:RS|Ksh|MRP)\\.?\\s?)(\\d+(:?\\,\\d+)?(\\,\\d+)?(\\.\\d{1,2})?)");
                Pattern pattern1 = Pattern.compile("(\\b\\d{10})");
                Matcher matcher1 = pattern1.matcher(data1);
                Matcher matcher = pattern.matcher(data);
                String val = "";
                String val2 = "";

                if (matcher.find() && (matcher1.find())) {
                    //for (int i = 0; i < matcher.groupCount() + 1; i++)
                    val = matcher.group(1);
                    val2 = matcher1.group(1);
                }
                double total=0.0;
                for(int i=0;i<mList.size();i++)
               total = total + Double.parseDouble(String.valueOf(mList.get(i).getAcc_no()));

               // SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                String address = cursor.getString(1);
                String body = cursor.getString(3);
                String type = cursor.getString(5);
                //String date = cursor.getString(cursor.getColumnIndexOrThrow("date")).toString();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Calendar calendar = Calendar.getInstance();
                long now = cursor.getLong(4);
                calendar.setTimeInMillis(now);
                String dateval = formatter.format(calendar.getTime());
                //sms.add(address+"\n\n"+body+"\n\n"+date);

                if(body.contains("sent")) {
                    mList.add(new model(val2,Double.parseDouble(val), dateval));
                    mytextview.setText(String.valueOf(total));

               }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mList;
    }
}

