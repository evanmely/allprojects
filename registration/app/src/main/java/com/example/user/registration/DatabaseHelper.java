package com.example.user.registration;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public abstract class DatabaseHelper extends SQLiteOpenHelper {
    public  static final  String DATABASE_NAME ="register.db";
    public static  final  String TABLE ="register";
    public static  final  String COL_1 ="ID";
    public static  final  String COL_2 ="first_name";
    public static  final  String COL_3 ="last_name";
    public static  final  String COL_4 ="email";
    public static  final  String COL_5 ="password";
 public DatabaseHelper(Context context){
     super(context,DATABASE_NAME,null,1);
 }
public void onCreate(SQLiteDatabase db){
db.execSQL("CREATE TABLE "+TABLE+"(ID INTEGER PRIMARY KEY  AUTOINCREMENT,first_name text,last_name text,email text,password text)");
}
public void onUpdate(SQLiteDatabase db,int old_version,int new_version){
db.execSQL("DROP TABLE IF EXISTS "+TABLE);//dropping table if there is one
    onCreate(db);
}
}
