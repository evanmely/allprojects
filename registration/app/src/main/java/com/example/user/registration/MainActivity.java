package com.example.user.registration;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

SQLiteOpenHelper openHelper;
SQLiteDatabase db;
private Button register,btnlogin;
private EditText myemail,mypassword;
private TextView login,infor;
private int counter=3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnlogin=(Button)findViewById(R.id.btnlogin);
        register=(Button)findViewById(R.id.btnregister);
        myemail=(EditText)findViewById(R.id.email);
        mypassword=(EditText)findViewById(R.id.password);
        infor=(TextView)findViewById(R.id.infor);
        login=(TextView)findViewById(R.id.login);
        infor.setText("Number of attempts Remaining: 3 ");
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate(myemail.getText().toString(),mypassword.getText().toString());
            }
        });
    }
    private void  validate(String username,String pass){
        if((username=="admin")&&(pass=="1234")){
            Intent intent=new Intent(MainActivity.this,register.class);
            startActivity(intent);
        }
        else{
            counter--;
            infor.setText("Number of Attempts Remaining: "+String.valueOf(counter));
            if(counter==0){
                btnlogin.setEnabled(false);
                          }
             }
    }
}
