package com.example.user.smartfarmer;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import java.sql.Statement;
import java.util.ArrayList;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;
public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String database_name = "aidb.sqlite";
    public static final String table_name = "users";
    Cursor cursor;

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //db.execSQL("CREATE TABLE users(id INTEGER PRIMARY KEY AUTOINCREMENT,name VARCHAR,email VARCHAR,location VARCHAR,password VARCHAR)");
    }

    public void addbullcatalogue(String insem_id,String breed, String name, String aicode, String longevity, String milk,String dam, String sire, String price, String fertility) {
        SQLiteDatabase database = getWritableDatabase();
        String sql = "INSERT INTO bulls VALUES(NULL,?,?,?,?,?,?,?,?,?,?)";
        SQLiteStatement statement = database.compileStatement(sql);
        statement.clearBindings();
        statement.bindString(1, insem_id);
        statement.bindString(2,breed);
        statement.bindString(3, name);
        statement.bindString(4, aicode);
        statement.bindString(5, longevity);
        statement.bindString(6, milk);
        statement.bindString(7, dam);
        statement.bindString(8, sire);
        statement.bindString(9, price);
        statement.bindString(10, fertility);
        statement.executeInsert();
    }

    public void querydata(String sql) {
        SQLiteDatabase database = getWritableDatabase();
        database.execSQL(sql);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + table_name);
        onCreate(db);
    }

    public void insertanimalsales(String category, String breed, String age, String price, byte[] image) {
        SQLiteDatabase database = getWritableDatabase();
        String sql = "INSERT INTO soldanimals VALUES(NULL,?,?,?,?,?)";
        SQLiteStatement statement = database.compileStatement(sql);
        statement.clearBindings();
        statement.bindString(1, category);
        statement.bindString(2, breed);
        statement.bindString(3, age);
        statement.bindString(4, price);
        statement.bindBlob(5, image);
        statement.executeInsert();
    }

    public Cursor getdata(String sql) {
        SQLiteDatabase database = getReadableDatabase();
        return database.rawQuery(sql, null);
    }

    public Cursor fetchdata(String breed,String sire,String dam) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT bulls.*,users.phone,users.location FROM bulls INNER JOIN users ON bulls.insem_id = users.id WHERE bulls.breed =? AND bulls.dam NOT IN(?) AND bulls.sire NOT IN(?)";
        Cursor data = db.rawQuery(query, new String[]{breed,dam,sire});
        return data;

    }
    public Cursor showdata(String email) {
        SQLiteDatabase database = getReadableDatabase();
        return database.rawQuery("SELECT id FROM users WHERE email=?",new String[] {email} );

    }
    public Cursor showtype(String email) {
        SQLiteDatabase database = getReadableDatabase();
        return database.rawQuery("SELECT * FROM users WHERE email=?",new String[] {email} );

    }

    public void addusers(String type, String myname, String myemail, String phone, String mylocation, String mypassword) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "INSERT INTO users VALUES(NULL,?,?,?,?,?,?)";
        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();
        statement.bindString(1, type);
        statement.bindString(2, myname);
        statement.bindString(3, myemail);
        statement.bindString(4, phone);
        statement.bindString(5, mylocation);
        statement.bindString(6, mypassword);
        statement.executeInsert();
    }

    public boolean verify(String username, String password) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("select * from users where email=? and password=?", new String[]{username, password});
        int count = c.getCount();
        if (count > 0)
            return true;

        else
            return false;

    }

    public void updateData(String breed, String age, String price, byte[] image, int id) {
        SQLiteDatabase database = getWritableDatabase();
        String sql = "UPDATE soldanimals SET breed =? ,age =? ,price =?, image =? WHERE id =? ";
        SQLiteStatement statement = database.compileStatement(sql);
        statement.bindString(1, breed);
        statement.bindString(2, age);
        statement.bindString(3, price);
        statement.bindBlob(4, image);
        statement.bindDouble(5, (double) id);
        statement.execute();
        database.close();
    }

    public void deleteData(int id) {
        SQLiteDatabase database = getWritableDatabase();
        String sql = "DELETE FROM soldanimals WHERE id=?";
        SQLiteStatement statement = database.compileStatement(sql);
        statement.clearBindings();
        statement.bindDouble(1, (double) id);
        statement.execute();
        database.close();
    }

    public void addinseminator(String type, String myname, String myemail, String location, String phone, String aicode, String password) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "INSERT INTO inseminators VALUES(NULL,?,?,?,?,?,?,?)";
        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();
        statement.bindString(1, type);
        statement.bindString(2, myname);
        statement.bindString(3, myemail);
        statement.bindString(4, location);
        statement.bindString(5, phone);
        statement.bindString(6, aicode);
        statement.bindString(7, password);
        statement.executeInsert();
    }

//    public ArrayList<inseminators> getusers() {
//        ArrayList<inseminator> arrayList = new ArrayList<>();
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery("SELECT * FROM users", null);
//        while (cursor.moveToNext()) {
//            int id = cursor.getInt(0);
//            String name = cursor.getString(1);
//            String phone = cursor.getString(2);
//            String location = cursor.getString(3);
//            inseminator inse = new inseminator(id, name, phone);
//            arrayList.add(inse);
//
//        }
//        return arrayList;
//    }

}