package com.example.user.smartfarmer;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Layout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
private ViewPager mslideviewpager;
private LinearLayout mDotlayout;
private  slider_adapter mslider;
private TextView []mDots;
private Button nextbtn,prevbtn;
public  int  current_page;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mslideviewpager= (ViewPager)findViewById(R.id.viewpager);
        mDotlayout=(LinearLayout)findViewById(R.id.dots);
        prevbtn=(Button)findViewById(R.id.btnprev) ;
        nextbtn= (Button)findViewById(R.id.btnnext);
        mslider =new slider_adapter(this);
        mslideviewpager.setAdapter(mslider);
         addDotsIndicator(0);
         mslideviewpager.addOnPageChangeListener(viewlistener);
         nextbtn.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 mslideviewpager.setCurrentItem(current_page+1);
             }
         });
        prevbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mslideviewpager.setCurrentItem(current_page-1);
            }
        });
    }
    public   void addDotsIndicator(int position){

       mDots = new TextView[3];
       mDotlayout.removeAllViews();
       for (int i = 0;i<mDots.length;i++){

           mDots[i]= new TextView(this);
           mDots[i].setText(Html.fromHtml("&#8226;"));
           mDots[i].setTextSize(35);
           mDots[i].setTextColor(getResources().getColor(R.color.colorWhite));
           mDotlayout.addView(mDots[i]);

       }
       if(mDots.length>0){
           mDots[position].setTextColor(getResources().getColor(R.color.colorAccent));
       }
    }
    ViewPager.OnPageChangeListener viewlistener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {

        }
        @Override
        public void onPageSelected(int i) {
        addDotsIndicator(i);
         current_page = i;
if(i == 0){
    prevbtn.setEnabled(false);
    nextbtn.setEnabled(true);
    prevbtn.setVisibility(View.INVISIBLE);
    nextbtn.setText("Next");
    prevbtn.setText("");
}else if(i == mDots.length -1){
    prevbtn.setEnabled(true);
    nextbtn.setEnabled(true);
    prevbtn.setVisibility(View.VISIBLE);
    nextbtn.setText("Got It ");
    prevbtn.setText("Back");
    nextbtn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent movetologin = new Intent(MainActivity.this,login_activity.class);
            startActivity(movetologin);
        }
    });

}
else{
    prevbtn.setEnabled(true);
    nextbtn.setEnabled(true);
    prevbtn.setVisibility(View.VISIBLE);
    nextbtn.setText("Next");
    prevbtn.setText("Back");
}
        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };


}


