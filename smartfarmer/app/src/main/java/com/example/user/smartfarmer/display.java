package com.example.user.smartfarmer;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class display extends AppCompatActivity {
    ListView lst;
    DatabaseHelper db;
    ArrayList<inseminators>mList;
    myadapter mAdapter = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        lst = (ListView) findViewById(R.id.list);
        mList = new ArrayList<>();
        mAdapter = new myadapter(this,R.layout.displayme_row,mList);
        lst.setAdapter(mAdapter);
        db = new DatabaseHelper(this, "aidb.sqlite", null, 1);
        try {
            Bundle y  = getIntent().getExtras();
            String breed1= y.getString("breed");
            String dam= y.getString("dam");
            String sire= y.getString("sire");

            Cursor cursor = db.fetchdata(breed1,dam,sire);
            mList.clear();
            while (cursor.moveToNext()){
                int id = cursor.getInt(1);
                String breed = cursor.getString(2);
                String name = cursor.getString(3);
                String price = cursor.getString(9);
                try {
                    Cursor c = db.getdata("SELECT * FROM users WHERE id ='"+id+"'");
                    while(c.moveToNext()) {
                        String location = c.getString(5);
                        String phone = c.getString(4);

                        mList.add(new inseminators(id, breed, name, price, location, phone));
                    }
                }
                catch(Exception e){
                    Toast.makeText(this, ""+e, Toast.LENGTH_SHORT).show();
                }
            }
            mAdapter.notifyDataSetChanged();
            if (mList.size() == 0) {
//if no record in the database  table
                Toast.makeText(this, "No Record Available", Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){
            Toast.makeText(this, ""+e, Toast.LENGTH_LONG).show();
        }
    }
}
