package com.example.user.smartfarmer;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class farmdash extends AppCompatActivity {
Button myservices,mynews,mymarket;
    private DrawerLayout mdrawerlayout;
    NavigationView navigation;
    private ActionBarDrawerToggle mtoggle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmdash);

        mdrawerlayout=(DrawerLayout)findViewById(R.id.drawerlayout);
        mtoggle = new ActionBarDrawerToggle(this,mdrawerlayout,R.string.open,R.string.close);
        mdrawerlayout.addDrawerListener(mtoggle);
        mtoggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        navigation=(NavigationView)findViewById(R.id.navigationview);
        navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener()
        {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id) {
                    case R.id.dashboard:

                        break;
                    case R.id.sell:
                        Intent sell =new Intent(farmdash.this,sell.class);
                        startActivity(sell);
                        break;
                    case R.id.aiservices:

                            Intent post = new Intent(farmdash.this, postdetails.class);
                            startActivity(post);

                        break;
                    case R.id.viewmarket:
                        Intent mrkt = new Intent(farmdash.this,RecordListActivity.class);
                        startActivity(mrkt);
                        break;
                    case R.id.logout:
                        Intent main = new Intent(farmdash.this,MainActivity.class);
                        startActivity(main);
                        break;
                }
                return false;
            }
        });
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mtoggle.onOptionsItemSelected(item)){
            return  true;
        }
        return super.onOptionsItemSelected(item);
    }

}
