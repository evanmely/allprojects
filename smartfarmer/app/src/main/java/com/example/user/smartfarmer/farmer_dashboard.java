package com.example.user.smartfarmer;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;

public class farmer_dashboard extends AppCompatActivity {
    private DrawerLayout mdrawerlayout;
    NavigationView navigation;
    private ActionBarDrawerToggle mtoggle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmer_dashboard);
        mdrawerlayout=(DrawerLayout)findViewById(R.id.mydrawerlayout);
        mtoggle = new ActionBarDrawerToggle(this,mdrawerlayout,R.string.open,R.string.close);
        mdrawerlayout.addDrawerListener(mtoggle);
        mtoggle.syncState();
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        navigation=(NavigationView)findViewById(R.id.navigationview);
        navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener()
        {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id) {
                    case R.id.dashboard:
                        break;
                    case R.id.sell:
                        Intent sell =new Intent(farmer_dashboard.this,sell_animals.class);
                        startActivity(sell);
                        break;
                    case R.id.aiservices:
                        Intent post = new Intent(farmer_dashboard.this,get_services.class);
                        startActivity(post);
                        break;
                    case R.id.viewmarket:
                        Intent mrkt = new Intent(farmer_dashboard.this,market_dash.class);
                        startActivity(mrkt);
                        break;
                    case R.id.logout:
                        Intent main = new Intent(farmer_dashboard.this,login_activity.class);
                        startActivity(main);
                        break;
                }
                return false;
            }
        });
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mtoggle.onOptionsItemSelected(item)){
            return  true;
        }
        return super.onOptionsItemSelected(item);
    }

}
