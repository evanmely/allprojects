package com.example.user.smartfarmer;

import android.database.Cursor;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class insemaddbulls extends AppCompatActivity {
    EditText txtid,myname,myaicode,mylongevity,myweight,mymilk,myprotein,mydam,mysire,myprice,myfertility;;
    DatabaseHelper db;
    Button btnsave;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insemaddbulls);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(" Bull Catalog");
        final Spinner myspinner =(Spinner)findViewById(R.id.spinner2);
        ArrayAdapter<String> myadapter1=new ArrayAdapter<>(insemaddbulls.this,android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.breed));
        myadapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        myspinner.setAdapter(myadapter1);
        txtid = (EditText)findViewById(R.id.id);
        Bundle z  = getIntent().getExtras();
        String email= z.getString("myemail");
        db = new DatabaseHelper(this,"aidb.sqlite",null,1);
        Cursor c = db.showdata(email);
        while(c.moveToNext()) {
            String value = c.getString(0);
            txtid.setText(value);
            txtid.setEnabled(false);
        }
        db.querydata("CREATE TABLE IF NOT EXISTS bulls(id INTEGER PRIMARY KEY AUTOINCREMENT,insem_id VARCHAR ,breed VARCHAR ,name VARCHAR,aicode VARCHAR,longevity INTEGER ,milk DOUBLE,dam VARCHAR,sire VARCHAR,price DOUBLE,fertility DOUBLE)");
        myname=(EditText)findViewById(R.id.name);
        myaicode=(EditText)findViewById(R.id.aicode);
        mylongevity=(EditText)findViewById(R.id.longevity);
        mymilk=(EditText)findViewById(R.id.milk);
        mydam=(EditText)findViewById(R.id.dam);
        mysire=(EditText)findViewById(R.id.sire);
        myprice=(EditText)findViewById(R.id.price);
        myfertility=(EditText)findViewById(R.id.fertility);
        btnsave=(Button)findViewById(R.id.save);


        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    db.addbullcatalogue(
                            txtid.getText().toString().trim(),
                            myspinner.getSelectedItem().toString().trim(),
                            myname.getText().toString().trim(),
                            myaicode.getText().toString().trim(),
                            mylongevity.getText().toString().trim(),
                            mymilk.getText().toString().trim(),
                            mydam.getText().toString().trim(),
                            mysire.getText().toString().trim(),
                            myprice.getText().toString().trim(),
                            myfertility.getText().toString().trim()
                    );

                    Toast.makeText(insemaddbulls.this, " Data Saved successfully", Toast.LENGTH_SHORT).show();
                    myname.setText("");
                    myaicode.setText("");
                    mydam.setText("");
                    myname.setText("");
                    myprotein.setText("");
                    myfertility.setText("");
                    myprice.setText("");
                    mysire.setText("");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
