package com.example.user.smartfarmer;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class inseminator extends AppCompatActivity {
    private DrawerLayout mdrawerlayout;

    NavigationView navigation;
    private ActionBarDrawerToggle mtoggle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inseminator);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(" Inseminator Dashboard");

        mdrawerlayout=(DrawerLayout)findViewById(R.id.mydrawerlayout);
        mtoggle = new ActionBarDrawerToggle(this,mdrawerlayout,R.string.open,R.string.close);
        mdrawerlayout.addDrawerListener(mtoggle);
        mtoggle.syncState();
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        navigation=(NavigationView)findViewById(R.id.mynav);
        navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener()
        {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id) {
                    case R.id.myaccount:

                        break;
                    case R.id.addservice:
                        Bundle y  = getIntent().getExtras();
                        String email = y.getString("username");
                        Intent sell =new Intent(getApplicationContext(),insemaddbulls.class);
                        Bundle b= new Bundle();
                        b.putString("myemail",email);
                        sell.putExtras(b);
                        startActivity(sell);
                        break;
                    case R.id.logout:
                        Intent main = new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(main);
                        break;
                }
                return false;
            }
        });

    }
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mtoggle.onOptionsItemSelected(item)){
            return  true;
        }
        return super.onOptionsItemSelected(item);
    }
    }

