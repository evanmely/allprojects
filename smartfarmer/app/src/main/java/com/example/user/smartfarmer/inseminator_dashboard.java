package com.example.user.smartfarmer;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

public class inseminator_dashboard extends AppCompatActivity {
    private DrawerLayout mdrawerlayout;
    NavigationView navigation;
    private ActionBarDrawerToggle mtoggle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inseminator_dashboard);
        mdrawerlayout=(DrawerLayout)findViewById(R.id.drawerlayout);
        mtoggle = new ActionBarDrawerToggle(this,mdrawerlayout,R.string.open,R.string.close);
        mdrawerlayout.addDrawerListener(mtoggle);
        mtoggle.syncState();
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        navigation=(NavigationView)findViewById(R.id.nav);
        navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener()
        {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id) {
                    case R.id.myaccount:

                        break;
                    case R.id.addservice:
                        Bundle y  = getIntent().getExtras();
                        String email = y.getString("myuser");
                        Intent sell =new Intent(getApplicationContext(),inseminator_add_bulls.class);
                        Bundle b= new Bundle();
                        b.putString("myemail",email);
                        sell.putExtras(b);
                        startActivity(sell);
                        break;
                    case R.id.logout:
                        Intent main = new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(main);
                        break;
                }
                return false;
            }
        });
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mtoggle.onOptionsItemSelected(item)){
            return  true;
        }
        return super.onOptionsItemSelected(item);
    }
}
