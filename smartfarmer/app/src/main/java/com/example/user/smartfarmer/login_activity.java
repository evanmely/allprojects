package com.example.user.smartfarmer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class login_activity extends AppCompatActivity {
    Button btnlogin,btnreg;
    DatabaseHelper db;
    TextView forgotpwd;
    EditText myemail ,mypassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnlogin=(Button)findViewById(R.id.login);
        btnreg=(Button) findViewById(R.id.btnregister);
        db=new DatabaseHelper(this,"aidb.sqlite",null,1);
        myemail=(EditText) findViewById(R.id.email);
        mypassword=(EditText)findViewById(R.id.password);
        btnlogin=(Button)findViewById(R.id.login);


        btnreg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent registerIntent=new Intent(login_activity.this,registration.class);
                startActivity(registerIntent);
            }
        });
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = myemail.getText().toString().trim();
                String pwd = mypassword.getText().toString().trim();
                try {
                    boolean test = db.verify(user, pwd);
                    if (test == true) {
                        //Boolean res = db.checkuser(user, pwd);
                        //if (user.equals("admin") && pwd.equals("pass")) {

                        //Toast.makeText(login_activity.this, "Admin", Toast.LENGTH_SHORT).show();
                        Intent movetoinsem =new Intent(getApplicationContext(),inseminator_dashboard.class);
                        Bundle b= new Bundle();
                        b.putString("myuser",user);
                        movetoinsem.putExtras(b);
                        startActivity(movetoinsem);
//                        Intent movetoinsem = new Intent(login_activity.this, inseminator_dashboard.class);
//                        startActivity(movetoinsem);
                    } else if(user.equals("farmer@gmail.com")&& pwd.equals("farmer")){
                        //if (res.equals(true) && item.equals("General public")) {
                        //Toast.makeText(MainActivity.this, "Login Successful-Welcome " + user, Toast.LENGTH_SHORT).show();

                       // Toast.makeText(login_activity.this, "Farm", Toast.LENGTH_SHORT).show();

                        Intent landing = new Intent(login_activity.this, farmer_dashboard.class);
                        startActivity(landing);

                        //} else {
                        //Toast.makeText(MainActivity.this, " Wrong  Username or Password ", Toast.LENGTH_SHORT).show();
                        //}

                    }
                    else {
                        Toast.makeText(login_activity.this, "Incorrect Credentials", Toast.LENGTH_SHORT).show();
                    }
                }
                catch(Exception r){
                    Toast.makeText(login_activity.this, "" + r, Toast.LENGTH_SHORT).show();
                }
            }

        });

    }
}
