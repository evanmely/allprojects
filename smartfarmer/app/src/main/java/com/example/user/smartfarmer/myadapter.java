package com.example.user.smartfarmer;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class myadapter extends BaseAdapter {
    private Context context;
    private  int layout;
    private ArrayList<inseminators> arrayList;

    public myadapter(Context context, int layout, ArrayList<inseminators> arrayList) {
        this.context = context;
        this.layout = layout;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    private  class ViewHolder{
        TextView txtbreed,txtname,txtphone,txtlocation,txtprice;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View row=view;
        myadapter.ViewHolder holder = new myadapter.ViewHolder();
        if(row==null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout,null);
            holder.txtbreed = row.findViewById(R.id.t_breed);
            holder.txtname = row.findViewById(R.id.t_name);
            holder.txtprice = row.findViewById(R.id.t_price);
            holder.txtlocation = row.findViewById(R.id.t_location);
            holder.txtphone = row.findViewById(R.id.t_phone);


             final  TextView text=(TextView)row.findViewById(R.id.t_phone);

            text.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    String phone_no= text.getText().toString().replaceAll("-", "");
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:"+phone_no));
                    context.startActivity(intent);

                }
            });
            row.setTag(holder);
        }else{
            holder = (ViewHolder)row.getTag();
        }
        inseminators model = arrayList.get(position);
        holder.txtbreed.setText(model.getBreed());
        holder.txtname.setText(model.getName());
        holder.txtprice.setText(model.getPrice());
        holder.txtlocation.setText(model.getLocation());
        holder.txtphone.setText(model.getPhone());
        return row;
    }
}
