package com.example.user.smartfarmer;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class postdetails extends AppCompatActivity {
EditText heifer,mydam,mysire;
Button btn_save;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_postdetails);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Post Heifer Details");
        final Spinner myspinner =(Spinner)findViewById(R.id.spinner2);
        ArrayAdapter<String> myadapter1 = new ArrayAdapter<>(postdetails.this,android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.breed));
        myadapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        myspinner.setAdapter(myadapter1);
        heifer =(EditText)findViewById(R.id.heifername);
        mydam =(EditText)findViewById(R.id.dam);
        mysire =(EditText)findViewById(R.id.sire);
        btn_save= (Button)findViewById(R.id.save);


       btn_save.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
       String breed =myspinner.getSelectedItem().toString().trim();
       String dam = mydam.getText().toString().trim();
       String sire= mysire.getText().toString().trim();

        Intent mysearch = new Intent(getApplicationContext(),Displayme.class);
        Bundle b = new Bundle();
        b.putString("breed",breed);
        b.putString("dam",dam);
        b.putString("sire",sire);
        mysearch.putExtras(b);
        startActivity(mysearch);
    }
});


    }
}
