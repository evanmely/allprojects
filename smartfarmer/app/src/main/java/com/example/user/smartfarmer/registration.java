package com.example.user.smartfarmer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class registration extends AppCompatActivity {
    EditText myname,myphone;
    EditText myemail;
    EditText mylocation;
    EditText pass;
    EditText cnfpass;
    RadioButton radiotype;
    TextView mylogin;
    DatabaseHelper db;
    Button mybtnsave;
    RadioGroup g1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);


        db = new DatabaseHelper(this, "aidb.sqlite", null, 1);
        db.querydata("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY AUTOINCREMENT,type VARCHAR , name VARCHAR,email VARCHAR,phone VARCHAR,location VARCHAR,password VARCHAR)");
        g1 = (RadioGroup)findViewById(R.id.G3);
        myname=(EditText)findViewById(R.id.name);
        myemail=(EditText)findViewById(R.id.email);
        mylocation=(EditText)findViewById(R.id.location);
        pass=(EditText)findViewById(R.id.password);
        myphone= (EditText)findViewById(R.id.phone);
        cnfpass=(EditText)findViewById(R.id.confirmpassword);
        mylogin=(TextView)findViewById(R.id.btnlogin);
        mybtnsave=(Button)findViewById(R.id.btnsave);
        myemail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                validatemail();
            }
        });
//        g1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                if (checkedId == R.id.inseminator) {
//                    Intent inse= new Intent(register.this, inseminator.class);
//                    startActivity(inse);
//                }
//                else{
//                  startActivity(new Intent(getApplicationContext(),register.class));
//                }
//            }
//        });
        mylogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent saveIntent = new Intent(registration.this,MainActivity.class);
                startActivity(saveIntent);
            }
        });
        mybtnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selected = g1.getCheckedRadioButtonId();
                radiotype = (RadioButton)findViewById(selected);
                String selection = radiotype.getText().toString().trim();
                String email=myemail.getText().toString().trim();
                String  name=myname.getText().toString().trim();
                String phoneno = myphone.getText().toString().trim();
                String location=mylocation.getText().toString().trim();
                String pwd=pass.getText().toString().trim();
                String cpwd=cnfpass.getText().toString().trim();
                if (email.equals("") || pwd.equals("") || mylocation.equals("")||myname.equals("")) {
                    Toast.makeText(registration.this, " Empty fields are not allowed", Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(registration.this, ""+radiotype.getText(), Toast.LENGTH_LONG).show();
                    try {
                        db.addusers(
                                selection,
                                name,
                                email,
                                phoneno,
                                location,
                                pwd
                        );

                        Toast.makeText(registration.this, " Data Saved successfully", Toast.LENGTH_LONG).show();
                        myemail.setText("");
                        mylocation.setText("");
                        myphone.setText("");
                        myname.setText("");
                        pass.setText("");
                        new Intent(getApplicationContext(),MainActivity.class);
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(),""+e,Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
    public boolean validatemail(){
        String emailInput = myemail.getText().toString().trim();
        if(emailInput.isEmpty()){
            myemail.setError("Empty fields not allowed");
            return false;
        }
        else if(!Patterns.EMAIL_ADDRESS.matcher(emailInput).matches()){
            myemail.setError("Email invalid");
            return false;
        }
        else {
            myemail.setError(null);
            return true;
        }
    }
    }

