package com.example.user.smartfarmer;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class slider_adapter extends PagerAdapter {
    Context mcontext;
    LayoutInflater layoutInflater;

    public slider_adapter(Context mcontext) {
        this.mcontext = mcontext;
    }
    public  int[] slide_images ={
            R.drawable.one,
            R.drawable.three,
            R.drawable.two

    };
    public  String [] slide_headings ={
           "About" ,
            "Inseminators",
           "Extra"

    };
    public  String [] slide_descriptions ={
            "The application helps individuals to view breeds and also be able to sell their livestock ,livestock productsamong others" ,
            " The inseminators also have the opportunity to register themselves and provide the details of their services",
            "To be the leading mobile application in helping farmers acquire what they want in terms of breeds and AI services  at their disposal"

    };

    @Override
    public int getCount() {
        return slide_headings.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view ==(RelativeLayout) o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
       layoutInflater = (LayoutInflater)mcontext.getSystemService(mcontext.LAYOUT_INFLATER_SERVICE);
       View view = layoutInflater.inflate(R.layout.slide_layout,container,false);
       ImageView mslideimageview = (ImageView)view.findViewById(R.id.slide_image);
        TextView  myheading = (TextView)view.findViewById(R.id.slide_heading);
        TextView  mydesc = (TextView)view.findViewById(R.id.slide_desc);
        mslideimageview.setImageResource(slide_images [position]);
        myheading.setText(slide_headings[position]);
        mydesc.setText(slide_descriptions[position]);
        container.addView(view);
       return  view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
       container.removeView((RelativeLayout)object);

    }


}
